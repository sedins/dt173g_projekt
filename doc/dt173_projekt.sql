/* Ta bort databasen dt173_projekt om den finns samt skapa en ny databas */
DROP DATABASE IF EXISTS dt173g_projekt;
CREATE DATABASE dt173g_projekt;
USE dt173g_projekt;

/* Create tables */
CREATE TABLE USER (
	Id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	Username VARCHAR(128) NOT NULL,
	Title VARCHAR(128) NOT NULL,
    Phone VARCHAR(128) NOT NULL,
    Email VARCHAR(128) NOT NULL,
    Password VARCHAR(256) NOT NULL,
    IsAdmin BIT(1) NOT NULL,
	IsActive BIT(1) NOT NULL,
	Created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Changed DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (Id),
	CONSTRAINT uc_email UNIQUE (Email)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE ITEM (
	Id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	UserId INT UNSIGNED NOT NULL,
	Heading VARCHAR(128) NOT NULL,
	Text VARCHAR(1024) NOT NULL,
	ImageUrl VARCHAR(128) NOT NULL,
	IsActive BIT(1) NOT NULL,
	Created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Changed DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (Id),
	KEY UserId (UserId),
	CONSTRAINT fk_item_user_1 FOREIGN KEY (UserId) REFERENCES USER (Id)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE LOG (
	Id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	UserId INT UNSIGNED NOT NULL,
	Script VARCHAR(512) NOT NULL,
	Message VARCHAR(4096) NOT NULL,
	Ip VARCHAR(32) NOT NULL,
	Created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (Id),
	KEY UserId (UserId),
	CONSTRAINT fk_log_user_1 FOREIGN KEY (UserId) REFERENCES USER (Id)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

/* Create data */
INSERT INTO USER (Id, Username, Title, Phone, Email, Password, IsAdmin, IsActive) VALUES
	(1, 'Stefan Hälldahl', 'Webbutvecklare', '070-1231000', 'stefan@halldahl.se', 'password', b'1', b'1');

INSERT INTO ITEM (Id, UserId, Heading, Text, ImageUrl, IsActive) VALUES
	(1, 1, 'DT057G - Webbutveckling I', 'I denna uppgift skapades en webbplats med tydligt innehåll och genomgående grafisk profil. Webbplatsen var av typen företagspresentation där fiktivt dito användes. Det gick även bra att skapa portfolio för att presentera sig som webbutvecklare samt samla kommande projektwebbplatser. Syftet med projektet är att omsätta kunskaper från de tidigare momenten i praktiken och i ett större sammanhängande projekt.', 'project_dt057g.jpg', b'1'),
	(2, 1, 'DT093G - Webbutveckling II', 'Syftet med projektet var att omsätta kunskaper från tidigare moment i praktiken samt i ett större sammanhängande projekt. En egen webbplats skall utarbetas med inriktning mot en vald målgrupp, grafisk profil, struktur/navigation, kombination av scriptspråk för att skapa en dynamisk webbplats, implementation som baseras på ett objektorienterat synsätt. Arbetet med webbplatsen kan med fördel pågå under så stor del av kursen som möjligt och gärna parallellt med övriga momenten.', 'project_dt093g.jpg', b'1'),
	(3, 1, 'DT068G - Webbanvändbarhet', 'I projektet skapades en webbplats med fokus på användbarhet. Denna blev även muntligt presenterad vid ett redovisningsseminarium samt skriftligt genom en projektrapport. I projektet var utgångspunkten inte nödvändigtvis att nya tekniker skulle användas, utan är istället att webbplatsen skapades efter designprocessen där användarcentrerad design tillämpades och ett antal användbarhetsmål bestämdes och utvärderades.', 'project_dt068g.jpg', b'1'),
	(4, 1, 'DT152G - Webbdesign för CMS', 'Här skapades en färdig webbplats med WordPress CMS som grund, utvecklad utifrån de kunskaper som erhållits under Moment 1 och 2 i kursen. Webbplatsen var av typen företagspresentation och ett fiktivt dito, tillsammans med tjänster/produkter. Kravet var att webbplatsen erbjöd någon form av produkter, som presenterades på lämpligt sätt. Det gick även att lägga produkter i varukorg och simulera köp via en äkta betalningslösning i testläge.', 'project_dt152g.jpg', b'1');
