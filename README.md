# DT173G - PROJEKT
___

## Inledning och syfte

Arbetet är tänkt att resultera i en webbplats som presenterar en portfolio över
utvalda projekt skapade av författaren genom programmet Webbutveckling vid
Mittuniversitetet. Idén att skapa en digital portfolio kom från ett tidigare moment
där vi skapade en webbplats med de kurser som ingick i programmet. Tanken
är att kunna presentera några av de projekt som genomförts. Webbplatsen
skulle även med enkelhet kunna utökas i senare skede att presentera ett fullständigt
CV om det skulle bli aktuellt.

Målsättningen är att göra webbsidorna väldigt enkla att navigera med överskådlig
och lättillgänglig information för användarna. Designmässigt bör den även
kännas modern och professionell då en del av dess funktion är att vara säljande.
Inloggad användare skall kunna lägga upp, ändra och ta bort de poster som presenteras.
Information kring användaren samt data om projekten hämtas och lagras
i databas och presenteras på webbplatsens förstasida.

Syftet med arbetet är att använda kunskaper från tidigare moment i kursen och
med dessa visa på förmåga att skapa en större sammanhängande webbplats.
Därför ska Git, Bitbucket, Gulp, SASS, TypeScript, webbtjänster med PHP och
databas med MySQL användas vid framtagandet av webbplatsen.

## Installation

Se medföljande dokument "doc/projekt.pdf" för instruktioner på installation och användande.

___

*Stefan Hälldahl, 2018-09-09*