// Scroll to top on page refresh
window.onbeforeunload = function () {
    window.scrollTo(0, 0);
}

$(document).ready(function() {

    /* Display message on top of screen if query (URL) parameter "note" exists */
    var note = getQueryValue("note");
    if (note) {
        displayNote(note);
    }

    /* Fill page with user data */
    ajaxCall("api/user.php?id=1", "GET").then(function(resp) {
        $('.name').text(resp.Username);
        $('.title').text(resp.Title);
        $('.email').attr("href", "mailto:" + resp.Email).find('span').text(" " + resp.Email);
        $('.phone').attr("href", "tel:" + resp.Phone).find('span').text(" " + resp.Phone);
    })
    
    /* Scroll to top on arrow up click */
    $('#top').click(function() {
        $('body,html').animate({
            scrollTop : 0
        }, 1000);
    });

    /* Initialize data on page load, click events and so on */
    if (document.URL.indexOf("login.html") > 0) { // Code for login page
        $('#submit').click(function(e) {
            e.preventDefault();
            var data = { email: $('#email').val().toString(), password: $('#password').val().toString() };
            ajaxCall("api/session.php", "POST", data).then(function(resp) {
                // Check if entered email and password exists and give appropriate feedback
                var note = (resp) ? "Användare inloggad" : "Felaktigt email eller lösenord angivet";
                window.location.href = "index.html?note=" + note;
            })
            return false;
        });
    } else if (document.URL.indexOf("edititem.html") > 0) { // Code for edit item page
        var id = getQueryValue("id").toString();
        // Init html fields with data from api
        if (id !== "" && id !== "0") {
            $('#formHeader').text('Ändra post');
            ajaxCall("api/item.php", "GET", { id: id }).then(function(resp) {
                $('#heading').val(resp.Heading);
                $('#text').val(resp.Text);
                $('#imageUrl').val(resp.ImageUrl);
            })
        } else {
            $('#formHeader').text('Ny post');
            $('#delete').css('display', 'none');
        }
        // Save changed data using rest api
        $('#submit').click(function(e) {
            e.preventDefault();
            var data = { id: id, heading: $('#heading').val().toString(), text: $('#text').val().toString(), imageUrl: $('#imageUrl').val().toString() };
            ajaxCall("api/item.php", "POST", data).then(function(resp) {
                var note = (resp) ? "Post sparad" : "Det gick inte att spara, försök igen senare";
                window.location.href = "index.html?note=" + note;
            })
            return false;
        })
        $('#delete').click(function(e) {
            var url = "api/item.php?id=" + id;
            ajaxCall(url, "DELETE").then(function(resp) {
                if (resp) {
                    window.location.href = "index.html?note=Post borttagen";
                }
            })
        })
    } else { // Code for main page
        var loggedIn = false;
        // Checks whether user is logged in or not and sets status accordingly
        ajaxCall("api/session.php", "GET").then(function(resp) {
            if (resp > 0) {
                loggedIn = true;
                $('.authorized').css('display', 'inline-block');
                $('#login i').removeClass().addClass('fa fa-sign-out-alt');
            } else {
                $('.authorized').css('display', 'none');
            }
        })
        // Change state on login button click
        $('#login').click(function(e) {
            if (loggedIn) {
                ajaxCall("api/session.php", "DELETE").then(function(resp) {
                    if (resp) {
                        loggedIn = false;
                        $('#login i').removeClass().addClass('fa fa-sign-in-alt');
                        $('.authorized').css('display', 'none');
                        displayNote("Användare utloggad");
                    }
                })
            } else {
                window.location.href = "login.html";
            }
        })
        // Scroll down on arrow down click
        $('#down').click(function() {
            $('html').scrollTop(0);
            $('html, body').animate({
                scrollTop: $('#infos').offset().top
            }, 1000);
        });
        ajaxCall("api/item.php", "GET").then(function(resp) {
            let html: string = "";
            resp.forEach(data => {
                var image = (!data.ImageUrl) ? "project_default.jpg" : data.ImageUrl;
                html += `<article><img alt="Bild på projekt" src="images/${image}"><div class="info"><h3>${data.Heading}</h3><p>${data.Text}</p></div>`;
                if (loggedIn) {
                    html += `<a href="edititem.html?id=${data.Id}" class="authorized"><i class="fa fa-edit" aria-hidden="true"></i></a>`;
                }
                html += `</article>`
            });
            $('#content').html(html);
        })
    }
});

/* Display note at top of screen */
function displayNote(note) {
    $("<div />", { class: 'message', text: decodeURI(note) }).hide().prependTo("body").slideDown('fast').delay(5000).slideUp(function() { $(this).remove(); });
}

/* Get query parameter from URL */
function getQueryValue(key) {
    var vars = window.location.search.substring(1).split("&");
    for (var i = 0; i < vars.length; i++) {
        var keyvalue = vars[i].split("=");
        if (keyvalue[0] === key) {
            return keyvalue[1];
        }
    }
    return "";
}

/* Generic ajax call used for communicating through rest api */
function ajaxCall(url: string, methodType: string, data: {[key: string]: string} = {}) {
    return $.ajax({
       url : url,
       data: data,
       method : methodType,
       dataType : "json"
    })
}
