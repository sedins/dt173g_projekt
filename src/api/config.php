<?php
    /*
    App configuration run at start

    Author: Stefan Hälldahl
    Date: 2018-10-01
    */

    /* Activate error reporting */
    error_reporting(E_ALL ^ E_NOTICE); // Report all errors except E_NOTICE
    ini_set("display_errors", 1);      // Display all errors

    /* Load classes automatically */
    spl_autoload_register(function ($class) {
        include 'classes/' . strtolower($class) . '.php';
    });

    /* Database settings */
    define("DBHOST", "192.168.1.10");
    define("DBUSERNAME", "database_username");
    define("DBPASSWORD", "database_password");
    define("DBDATABASE", "dt173g_projekt");

    /* Initialize classes */
    $session = new DbSession();
    $user = new DbUser();
    $item = new DbItem();
    $log = new DbLog();