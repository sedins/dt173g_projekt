<?php
/*
REST API with actions to fetch data returned as JSON and persist information to database

Author: Stefan Hälldahl
Date: 2018-10-01
*/

include("config.php");

header('content-type: application/json; charset=utf-8');
header('access-control-allow-origin: *');

switch($_SERVER['REQUEST_METHOD']) {
    case "GET":
        if (!empty($_GET['id'])) {
            $result = $item->getItem($_GET['id']);
        } else {
            $result = $item->getItems();
        }
        break;
    case "POST":
        if (empty($_POST['id']) || $_POST['id'] == 0) {
            $result = $item->addItem($session->getUserId(), $_POST['heading'], $_POST['text'], $_POST['imageUrl']);
        } else {
            $result = $item->updateItem($session->getUserId(), $_POST['id'], $_POST['heading'], $_POST['text'], $_POST['imageUrl']);
        }
        break;
    case "DELETE":
        if (!empty($_GET['id'])) {
            $result = $item->deleteItem($_GET['id']);
        }
        break;
    default:
        // Do nothing
}

$json = json_encode($result, JSON_PRETTY_PRINT);
echo $json;
