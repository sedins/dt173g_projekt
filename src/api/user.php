<?php
/*
REST API with actions to fetch data returned as JSON and persist information to database

Author: Stefan Hälldahl
Date: 2018-10-01
*/

include("config.php");

header('content-type: application/json; charset=utf-8');
header('access-control-allow-origin: *');

switch($_SERVER['REQUEST_METHOD']) {
    case "GET":
        if (!empty($_GET['id'])) {
            $result = $user->getUser($_GET['id']);
        } else {
            $result = $user->getUsers();
        }
        break;
    case "DELETE":
        if (!empty($_GET['id'])) {
            $result = $user->deleteUser($_GET['id']);
        }
        break;
    default:
        // Do nothing
}

$json = json_encode($result, JSON_PRETTY_PRINT);
echo $json;
