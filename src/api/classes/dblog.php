<?php

/*
Fetches and persists log messages

Author: Stefan Hälldahl
Date: 2018-10-01
*/

class DbLog extends DbBase {

    /* Add log message and information to database */
    public function addLog($message) {
        $userid = isset($_SESSION["userid"]) ? $_SESSION["userid"] : 0;
        $script = basename($_SERVER['REQUEST_URI']);
        $stmt = $this->db->prepare("INSERT INTO LOG (UserId, Script, Message, Ip) VALUES (?, ?, ?, ?)");
        $stmt->bind_param("ssss", $userid, $script, $message, $_SERVER['REMOTE_ADDR']);
        return $stmt->execute();
    }

    /* Load logs from database, populate array and return result */
    public function getLogs($maxrows = PHP_INT_MAX) {
        $stmt = $this->db->prepare("SELECT l.*, u.Username FROM LOG l INNER JOIN USER u ON u.id = l.userid ORDER BY l.created DESC LIMIT ?");
        $stmt->bind_param("i", $maxrows);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }
}
