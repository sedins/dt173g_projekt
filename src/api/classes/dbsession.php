<?php

/*
Handles information needed while the user session is alive

Author: Stefan Hälldahl
Date: 2018-10-04
*/

interface IDbSession {
    public function getUserId();
    public function setUserId($id);
    public function logout();
    public function isAdmin();
    public function isLoggedIn();
}

class DbSession implements IDbSession {

    /* Start session */
    public function __construct() {
        session_start();
    }

    /* Get userid associated with this session */
    public function getUserId() {
        return (isset($_SESSION["userid"]) ? $_SESSION["userid"] : -1);
    }

    /* Save userid that will be associated with this session */
    public function setUserId($id) {
        $_SESSION["userid"] = $id;
        return true;
    }

    /* Logout current user */
    public function logout() {
        $this->setUserId(-1);
        return true;
    }

    /* Is current user administrator */
    public function isAdmin() {
        $user = new DbUser();
        return $user->getUser($this->getUserId())['IsAdmin'];
    }

    /* Is current user logged in */
    public function isLoggedIn() {
        return (strlen($this->getUserId()) > 0);
    }

    /* Destroy session */
    public function destroy() {
        session_destroy();
    }
}
