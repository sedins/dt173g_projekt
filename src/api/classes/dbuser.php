<?php

/*
Fetches and persists users

Author: Stefan Hälldahl
Date: 2018-10-04
*/

class DbUser extends DbBase {

    /* Add user to database */
    public function addUser($username, $title, $phone, $email, $password) {
        $stmt = $this->db->prepare("INSERT INTO USER (Username, Title, Phone, Email, Password, IsAdmin) VALUES (?, ?, ?, ?, ?, 0)");
        $stmt->bind_param("sssss", $username, $title, $phone, $email, $password);
        return $stmt->execute();
    }

    /* Update user in database */
    public function updateUser($id, $username, $title, $phone, $email, $password) {
        $stmt = $this->db->prepare("UPDATE USER SET Username = ?, Title = ?, Phone = ?, Email = ?, Password = ?, Changed = NOW() WHERE Id = ?");
        $stmt->bind_param("sssssi", $username, $title, $phone, $email, $password, $id);
        return $stmt->execute();
    }

    /* Validate given email and password, used at login */
    public function validateUser($email, $password) {
        $stmt = $this->db->prepare("SELECT * FROM USER WHERE Email = ? AND Password = ?");
        $stmt->bind_param("ss", $email, $password);
        $stmt->execute();
        $stmt->store_result();
        return ($stmt->num_rows === 1);
    }

    /* Delete specified user from database */
    public function deleteUser($id) {
        $stmt = $this->db->prepare("UPDATE USER SET IsActive = false WHERE Id = ?");
        $stmt->bind_param("i", $id);
        return $stmt->execute();
    }

    /* Update password for specified user */
    public function updatePassword($id, $oldpassword, $newpassword) {
        $who = $this->getUser($id);
        if ($this->validateUser($who['Email'], $oldpassword)) {
            $stmt = $this->db->prepare("UPDATE USER SET Password = ?, Changed = NOW() WHERE Id = ?");
            $stmt->bind_param("si", $newpassword, $id);
            return $stmt->execute();
        }
        return false;
    }

    /* Get user information from database */
    public function getUser($id) {
        $stmt = $this->db->prepare("SELECT * FROM USER WHERE IsActive = true AND Id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        return $stmt->get_result()->fetch_assoc();
    }

    /* Get user information from database */
    public function getUserByEmail($email) {
        $stmt = $this->db->prepare("SELECT * FROM USER WHERE IsActive = true AND Email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        return $stmt->get_result()->fetch_assoc();
    }

    /* Load users from database, populate array and return result */
    public function getUsers() {
        $sql = "SELECT * FROM USER WHERE IsActive = true";
        $result = $this->db->query($sql);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }
}
