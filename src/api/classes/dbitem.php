<?php

/*
Fetches and persists items

Author: Stefan Hälldahl
Date: 2018-10-01
*/

class DbItem extends DbBase {

    /* Add item to database */
    public function addItem($userid, $heading, $text, $imageurl) {
        if ($userid > 0) {
            $stmt = $this->db->prepare("INSERT INTO ITEM (UserId, Heading, Text, ImageUrl, IsActive) VALUES (?, ?, ?, ?, 1)");
            $stmt->bind_param("isss", $userid, $heading, $text, $imageurl);
            return $stmt->execute();
        } else {
            return false;
        }
    }

    /* Update item in database */
    public function updateItem($userid, $id, $heading, $text, $imageurl) {
        if ($userid > 0) {
            $id = intval($id); // Only accept integers
            $stmt = $this->db->prepare("UPDATE ITEM SET Heading = ?, Text = ?, ImageUrl = ?, Changed = NOW() WHERE Id = ?");
            $stmt->bind_param("sssi", $heading, $text, $imageurl, $id);
            return $stmt->execute();
        } else {
            return false;
        }
    }

    /* Delete specified item from database */
    public function deleteItem($id) {
        $id = intval($id); // Only accept integers
        $stmt = $this->db->prepare("UPDATE ITEM SET IsActive = false WHERE Id = ?");
        $stmt->bind_param("i", $id);
        return $stmt->execute();
    }

    /* Get item information from database */
    public function getItem($id) {
        $id = intval($id); // Only accept integers
        $stmt = $this->db->prepare("SELECT i.*, u.Username, u.Phone, u.Email FROM ITEM i INNER JOIN USER u ON u.Id = i.UserId WHERE i.IsActive = true AND i.Id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        return $stmt->get_result()->fetch_assoc();
    }

    /* Load items from database, populate array and return result */
    public function getItems() {
        $sql = "SELECT i.*, u.Username, u.Phone, u.Email FROM ITEM i INNER JOIN USER u ON u.Id = i.UserId WHERE i.IsActive = true";
        $result = $this->db->query($sql);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }
}
