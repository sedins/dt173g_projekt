<?php

/*
Base class for database handling

Author: Stefan Hälldahl
Date: 2018-10-01
*/

abstract class DbBase {

    protected $db;

    public function __construct() {
        $this->db = new mysqli(DBHOST, DBUSERNAME, DBPASSWORD, DBDATABASE);
        if ($this->db->connect_errno > 0) {
            die('Anslutning till databasen misslyckades: ' . $this->db->connect_error);
        }
    }
}