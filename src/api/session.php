<?php

/*
Handles information needed while the user session is alive

Author: Stefan Hälldahl
Date: 2018-10-04
*/

include("config.php");

header('content-type: application/json; charset=utf-8');
header('access-control-allow-origin: *');

switch($_SERVER['REQUEST_METHOD']) {
    case "GET":
        $result = $session->getUserId();
        break;
    case "POST":
        $result = false;
        if (!empty($_POST['email']) && !empty($_POST['password'])) {
            if ($user->validateUser($_POST['email'], $_POST['password'])) {
                $who = $user->getUserByEmail($_POST['email']);
                $session->setUserId($who['Id']);
                $result = true;
            }
        }
        break;
    case "DELETE":
        $result = $session->logout();
        break;
    default:
        // Do nothing
}

$json = json_encode($result, JSON_PRETTY_PRINT);
echo $json;
