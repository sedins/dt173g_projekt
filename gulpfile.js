var gulp = require('gulp'),
    sass = require('gulp-sass'),
    inject = require('gulp-inject'),
    php = require('gulp-connect-php'),
    browserSync = require('browser-sync').create(),
    csso = require('gulp-csso'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    ts = require('gulp-typescript'),
    tsconfig = require("./src/tsconfig.json");

gulp.task('default', ['serve']);
gulp.task('build', ['injectDist']);

/* gulp serve: Starts a service that synchronizes the development environment
               with the default browser */
gulp.task('serve', ['watch'], function() {
    php.server({
        base: 'dev',
        port: 8010,
        keepalive: true
    }, function () {
        browserSync.init({
            proxy: '127.0.0.1:8010',
            port: 8080,
            startPath: '/index.html',
            open: true,
            notify: false,
            reloadDelay: 1000
        })
    })
});

/* gulp watch: Watches filesystem for changes to CSS, JS, HTML and JPG-files
               and updates development environment accordingly */
gulp.task('watch', ['sassDev', 'tsDev', 'injectDev'], function () {
    gulp.watch('src/scss/**/*.scss', ['sassDev']);
    gulp.watch('src/scripts/**/*.ts', ['tsDev']);
    gulp.watch('src/**/*.{php,html,js,jpg,png,svg}', ['injectDev']).on('change', function () {
        browserSync.reload(); // Uppdatera webbläsare om filer ändras
    });
});

/* gulp injectDev: Injects HTML with references to JS- and CSS-files */
gulp.task('injectDev', ['copyDev'], function () {
    return gulp.src('dev/**/*.html')
        .pipe(inject(gulp.src('dev/**/*.css', { read: false }), { relative: true } ))
        .pipe(inject(gulp.src('dev/libs/**/*.js', { read: false }), { name: 'inject-libs', relative: true } ))
        .pipe(inject(gulp.src('dev/scripts/**/*.js', { read: false }), { name: 'inject-scripts', relative: true } ))
        .pipe(gulp.dest('dev'));
});

/* gulp copyDev: Copies source files to the development environment */
gulp.task('copyDev', ['sassDev', 'tsDev'], function () {
    return gulp.src('src/**/*.{php,html,js,css,jpg,png,svg,ico,eot,ttf,woff,woff2}').pipe(gulp.dest('dev'));
});

/* gulp sassDev: Converts SCSS-files to CSS, copies output to the CSS-directory
                 and reloads the browser */
gulp.task('sassDev', function() {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sass())                     // Konvertera alla SASS-filer till CSS...
        .pipe(gulp.dest('dev/css'))       // ...och lägg resultatet i katalogen dev/css
        .pipe(browserSync.reload({        // Uppdatera webbläsare om CSS-filer ändrats
            stream: true
        }))
});

gulp.task('tsDev', function () {
    return gulp.src('src/**/*.ts')
        .pipe(ts(tsconfig.compilerOptions))
        .pipe(gulp.dest('dev'));
});

/* gulp injectDist: Injects HTML with references to JS- and CSS-files */
gulp.task('injectDist', ['copyDist'], function () {
    return gulp.src('dist/**/*.html')
        .pipe(inject(gulp.src('dist/**/*.css', { read: false }), { relative: true } ))
        .pipe(inject(gulp.src('dist/libs/**/*.js', { read: false }), { name: 'inject-libs', relative: true } ))
        .pipe(inject(gulp.src('dist/scripts/**/*.js', { read: false }), { name: 'inject-scripts', relative: true } ))
        .pipe(gulp.dest('dist'));
});

/* gulp copyDist: Copies HTML- and JPG-files to the production environment */
gulp.task('copyDist', ['sassDist', 'tsDist'], function () {
    return gulp.src('src/**/*.{php,html,js,css,jpg,png,svg,ico,eot,ttf,woff,woff2}').pipe(gulp.dest('dist'));
});

/* gulp sassDist: Convert, merge and minify SCSS-files to one CSS-file
                     before copying the output to the CSS-directory */
gulp.task('sassDist', function () {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sass())                  // Konvertera alla SASS-filer till CSS...
        .pipe(concat('main.min.css'))
        .pipe(csso())
        .pipe(gulp.dest('dist/css'));
});

/* gulp tsDist: Convert TS-files to JS and minify into one JS-file before
                    copying the output to the JS-directory */
gulp.task('tsDist', function () {
    return gulp.src('src/**/*.ts')
        .pipe(ts(tsconfig.compilerOptions))
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/scripts'));
});
